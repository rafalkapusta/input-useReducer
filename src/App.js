import React, { useReducer } from 'react'
//import logo from './logo.svg';
//import './App.css';

const initialState = {
  name: '',
  surname: '',
  email: '',
}

const reducer = (state, { name, value, type }) => {
  switch (type) {
    case 'INPUT':
      return {
        ...state,
        [name]: value,
      }
    case 'CLEAR':
      return {
        ...initialState
      }
    default:
      return state
  }
}

function App () {
  const [state, dispatch] = useReducer(reducer, initialState)

  const { name, surname, email } = state

  const handleInputChange = event => {
    dispatch({ name: event.target.name, value: event.target.value, type: 'INPUT' })
  }

  const handleClearForm = () => dispatch({type: 'CLEAR'})
  return (
    <>
      <h1>Inputy kontrolowane - useReducer</h1>
      <input type="text"
             name='name'
             placeholder='imię'
             value={name}
             onChange={(event) => handleInputChange(event)}/>
      <input type="text"
             name='surname'
             placeholder='nazwisko'
             value={surname}
             onChange={(event) => handleInputChange(event)}/>
      <input type="text"
             name='email'
             placeholder='email'
             value={email}
             onChange={(event) => handleInputChange(event)}/>
      <button onClick={handleClearForm}>Wyczyść formularz</button>
    </>

  )
}

export default App
